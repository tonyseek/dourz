import json

from py.path import local
from pytest import fixture

from dourz.douban.session import DoubanSession
from dourz.douban.reader import Book
from dourz.exports.epub.render import BookContext


@fixture
def book():
    book_json = local(__file__).dirpath('data/book.json').read()
    book_vars = json.loads(book_json)
    return Book(DoubanSession(), **book_vars)


@fixture
def book_data():
    book_json = local(__file__).dirpath('data/book_data.json').read()
    return json.loads(book_json)


@fixture
def ctx(book, book_data):
    return BookContext(book, book_data)


def test_render(ctx):
    assert set(ctx.metadata) == {
        'content.opf', 'toc.ncx', 'container.xml', 'mimetype'}
    assert ctx.metadata['mimetype'] == b'application/epub+zip'
    assert ctx.metadata['container.xml'] != b''
    assert ctx.metadata['toc.ncx'] != b''
    assert ctx.metadata['content.opf'] != b''

    assert b'<itemref idref="chapter-0" />' in ctx.metadata['content.opf']
    assert b'<itemref idref="chapter-1" />' in ctx.metadata['content.opf']
    assert b'href="chapter-0.xhtml"' in ctx.metadata['content.opf']
    assert b'href="chapter-1.xhtml"' in ctx.metadata['content.opf']

    assert b'<content src="chapter-0.xhtml"/>' in ctx.metadata['toc.ncx']
    assert b'<content src="chapter-1.xhtml"/>' in ctx.metadata['toc.ncx']

    all_files = dict(ctx.render_all())
    assert set(all_files) == {
        'mimetype',
        'META-INF/container.xml',
        'Content/toc.ncx',
        'Content/content.opf',
        'Content/chapter-0.xhtml',
        'Content/chapter-1.xhtml',
        'Content/Image/cover.jpg'}
    assert all(all_files.values())
