from py.path import local

from dourz.douban.dml import (AnnotationParser, EmphasisParser,
                              DoubanMarkupParser)
from dourz.utils import safe_to_unicode


def data(parser_name, kind=None):
    """Read the test data from text file."""
    if kind:
        filename = '%s_%s' % (parser_name, kind)
    else:
        filename = parser_name
    data_bytes = local(__file__).dirpath('data/dml_%s.txt' % filename).read()
    return safe_to_unicode(data_bytes)


def test_annotation():
    parser = AnnotationParser(footnotes=[])
    assert parser.parse(data('annotation')) == data('annotation', 'result')
    assert parser.footnotes[0] == data('annotation', 'footnote').strip()


def test_complex_annotation():
    parser = AnnotationParser(footnotes=[])
    complex_text = data('annotation') * 3
    first_result = data('annotation', 'result')
    complex_result = ''.join([
        first_result,

        first_result
        .replace('#footnote-0', '#footnote-1')
        .replace('[1]', '[2]')
        .replace('footnote-ref-0', 'footnote-ref-1'),

        first_result
        .replace('#footnote-0', '#footnote-2')
        .replace('[1]', '[3]')
        .replace('footnote-ref-0', 'footnote-ref-2'),
    ])
    assert parser.parse(complex_text) == complex_result
    assert parser.footnotes == [data('annotation', 'footnote').strip()] * 3


def test_emphasis():
    parser = EmphasisParser()
    assert parser.parse(data('emphasis')) == data('emphasis', 'result')


def test_integration():
    parser = DoubanMarkupParser()
    origin = data('annotation') + data('emphasis')
    result = data('annotation', 'result') + data('emphasis', 'result')
    footnote = data('annotation', 'footnote').strip()
    assert parser.parse(origin) == result
    assert parser['annotation'].footnotes == [footnote]
