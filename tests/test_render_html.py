import json

from py.path import local
from pytest import fixture

from dourz.exports.html.render import BookRender


@fixture
def book():
    book_json = local(__file__).dirpath('data/book_data.json').read()
    return json.loads(book_json)


@fixture
def book_render(book):
    return BookRender(book)


def test_render_chapter(book_render):
    html_a = book_render.render_chapter_with_index(0)
    assert u'\u597d\u5427' in html_a

    html_b = book_render.render_chapter(book_render.chapters[0])
    assert html_b == html_a

    html_c = next(book_render.render_chapters())
    assert html_c == html_a
