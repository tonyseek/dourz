from setuptools import setup, find_packages


setup(
    name='dourz',
    packages=find_packages(exclude=['tests', 'docs']),
    package_data={
        'dourz.exports.epub': ['templates/*'],
        'dourz.exports.html': ['templates/*.xhtml', 'templates/contents/*'],
    },
    version='0.8.0',
    description='Delivery books from your read.douban.com account to your '
                'Kindle devices.',
    long_description='',
    author='Jiangge Zhang',
    author_email='tonyseek@gmail.com',
    url='https://bitbucket.org/tonyseek/dourz',
    license='Private',
    keywords=['douban', 'kindle'],
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Environment :: Other Environment',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    install_requires=[
        'Wand',
        'lxml',
        'requests',
        'Jinja2',
        'six',
        # Caused by matplotlib's broken support for OS X non-framework Python
        'matplotlib < 1.5',
        'click',
    ],
    entry_points={
        'console_scripts': [
            'dourz = dourz.cli:cli',
        ],
    },
)
