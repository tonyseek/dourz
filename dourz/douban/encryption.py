import re

from six import unichr


def chunks(iterable, chunk_size):
    """The chunks iterator."""
    _list = list(iterable)
    for i in range(0, len(_list), chunk_size):
        chunked = _list[i:i + chunk_size]
        if len(chunked) < 4:
            chunked = chunked + (4 - len(chunked)) * [0]
        yield tuple(chunked[:4])


class Hex64(object):
    """The hex64 decoder."""

    CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz$_~"
    re_clean = re.compile(r"[^0-9A-Za-z$_~]")

    def __init__(self, secret_key):
        assert len(secret_key) == 64, "the length of secret key must be 64"
        chars_key = (self.CHARS[pos] for pos in secret_key)
        self.table = {v: k for k, v in enumerate(chars_key)}

    def string_to_position(self, string):
        string = self.re_clean.sub(u"", string)  # clean
        return [self.table.get(char, 0) for char in string]

    def decrypt(self, string):
        position = self.string_to_position(string)
        for n1, n2, n3, n4 in chunks(position, 4):
            yield n1 << 2 | n2 >> 4
            yield (15 & n2) << 4 | n3 >> 2
            yield (3 & n3) << 6 | n4

    def one_to_two(self, iterable):
        for ord_ in iterable:
            yield ord_ * 256 + next(iterable)

    def decode(self, value):
        value = u"".join(map(unichr, self.one_to_two(self.decrypt(value))))
        value = value.strip(u"\0")  # remove tails zero bytes
        return value


def douban_hex64():
    """Creates hex64 for decoding douban books."""
    key = [37, 7, 20, 41, 59, 53, 8, 24, 5, 62, 31, 4, 32, 6, 50, 36, 63, 35,
           51, 0, 13, 43, 46, 40, 15, 27, 17, 57, 28, 54, 1, 60, 21, 22, 47,
           42, 30, 39, 12, 3, 9, 45, 29, 23, 56, 2, 16, 61, 52, 44, 25, 14,
           49, 34, 33, 10, 58, 55, 19, 26, 11, 18, 48, 38]
    return Hex64(key)
