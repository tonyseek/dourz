import collections
import random

import requests
import lxml.html
import wand.image
from requests.utils import dict_from_cookiejar, cookiejar_from_dict
from six.moves.urllib.parse import urlparse

from . import consts


CaptchaSolution = collections.namedtuple('CaptchaSolution', ['id', 'solution'])


class CaptchaImage(collections.namedtuple('CaptchaImage', ['id', 'url'])):
    """The captcha image."""

    def bind_session(self, session):
        self._session = session

    def make_image(self):
        if not hasattr(self, '_bytes'):
            self._bytes = self.download_image()
        return wand.image.Image(blob=self._bytes)

    def download_image(self):
        try:
            session = self._session
        except AttributeError:
            raise RuntimeError('session is unbound')
        else:
            response = session.get(self.captcha_url)
            response.raise_for_status()
            return response.content


class DoubanSession(object):
    """The login session for douban account."""

    def __init__(self):
        self.requests = requests.Session()
        self.requests.headers['User-Agent'] = \
            random.choice(consts.BROWSER_USERAGENTS)
        self._is_authorized = False

    def load_cookies(self, cookies):
        self.requests.cookies = cookiejar_from_dict(cookies)

    def dump_cookies(self):
        return dict_from_cookiejar(self.requests.cookies)

    @property
    def is_authorized(self):
        """Check current session is authorized or not."""
        if self._is_authorized:
            return True
        if self.fetch_uname():
            self._is_authorized = True
        return self._is_authorized

    def fetch_uname(self):
        """Fetch user's douban id."""
        response = self.requests.get(consts.MINE_URL)
        response.raise_for_status()
        mine_url = urlparse(response.url)
        if mine_url.path.startswith('/people/'):
            return mine_url.path[len('/people/'):].strip('/')
        if mine_url.path.startswith('/accounts/login'):
            return
        raise RuntimeError('unknown response')

    def logout(self, clear_all=False):
        if clear_all:
            self.requests.cookies.clear()
        else:
            for key in ['ck', 'ue', 'dbcl2']:
                self.requests.cookies.pop(key, None)

    def login(self, username, password, captcha_solution=None):
        """Login a douban account.

        :param username: the douban login identity.
        :param password: the plain text password of current account.
        """
        data = {'source': 'index_nav',
                'redir': consts.HOME_URL,
                'form_email': username,
                'form_password': password[:20],
                'login': consts.LOGIN_BTN_TEXT,
                'remember': 'on'}
        if captcha_solution:
            data['captcha-id'] = captcha_solution.id
            data['captcha-solution'] = captcha_solution.solution

        response = self.requests.post(
            consts.LOGIN_URL, data=data, headers={'referer': consts.LOGIN_URL})
        etree = lxml.html.fromstring(response.text)

        # errors occured
        errors = etree.xpath('//div[@id="item-error"]/p/text()')
        if errors:
            raise LoginError(errors)

        # require to provide captcha
        captcha_url = ''.join(etree.xpath('//img[@id="captcha_image"]/@src'))
        captcha_id = ''.join(etree.xpath('//input[@name="captcha-id"]/@value'))
        if captcha_id and captcha_url:
            if captcha_solution:
                self.requests.cookies = response.cookies  # Remove cookies
            captcha = CaptchaImage(captcha_id, captcha_url)
            raise CaptchaRequiredError(captcha, username, password)

    @property
    def ck(self):
        """The CSRF token."""
        return self.requests.cookies.get('ck').strip('"')


class DoubanSessionMixin(object):
    """The collection of shortcuts for douban session related classes."""

    @property
    def is_authorized(self):
        return self.session.is_authorized

    @property
    def requests(self):
        if not self.is_authorized:
            raise UnauthorizedError()
        return self.session.requests

    @property
    def ck(self):
        if not self.is_authorized:
            raise UnauthorizedError()
        return self.session.ck


def session_related(factory_method_name):
    """Register a class as session related.

    Example::

        @session_related('frodo')
        class DoubanFrodo(object):
            def __init__(self, session, **kwargs):
                self.session = session
                self.kwargs = kwargs

        session = DoubanSession()
        session.frodo(foo=1)  # equals to `DoubanFrodo(session, foo=1)`
    """
    def decorator(cls):
        # make shortcut method
        def fn(self, *args, **kwargs):
            return cls(self, *args, **kwargs)
        fn.__name__ = fn.func_name = factory_method_name
        # assign to douban session class
        setattr(DoubanSession, factory_method_name, fn)
        return cls
    return decorator


class CaptchaRequiredError(Exception):
    """Failed to login because the captcha is required."""

    def __init__(self, captcha=None, username=None, password=None):
        self.captcha = captcha
        self.username = username
        self.password = password


class LoginError(Exception):
    """Failed to login because of some unknown errors."""

    def __init__(self, errors):
        self.errors = errors


class UnauthorizedError(Exception):
    """The request must be authorized."""
