import re
import collections

from markupsafe import Markup, escape

from dourz.utils import safe_to_unicode, render_latex, sha1


class DoubanMarkupParser(object):
    """The parser for douban markup language."""

    def __init__(self):
        self.footnotes = []
        self.images = {}
        self.parsers = collections.OrderedDict([
            ('metadata', MetadataParser(self.footnotes, self.images)),
            ('annotation', AnnotationParser(self.footnotes)),
            ('emphasis', EmphasisParser()),
        ])

    def parse(self, text):
        for parser in self.parsers.values():
            text = parser.parse(text)
        return text

    def __getitem__(self, name):
        return self.parsers[name]


class ElementParser(object):
    label_begin = u'\u5f00\u59cb'
    label_end = u'\u7ed3\u675f'

    def make_pattern(self):
        markup_begin = escape(u'<{0.label_type}{0.label_begin}>'.format(self))
        markup_end = escape(u'</{0.label_type}{0.label_end}>'.format(self))
        return re.compile(u'{0}(.+?){1}'.format(markup_begin, markup_end))

    def parse(self, text):
        pattern = self.make_pattern()
        text = escape(safe_to_unicode(text))
        return Markup(pattern.sub(self.process_match, text))


class AnnotationParser(ElementParser):
    label_type = u'\u6ce8\u91ca'
    markup = Markup(
        u'<sup id="footnote-ref-{0}">'
        u'<a class="footnote" href="#footnote-{0}">[{1}]</a></sup>')

    def __init__(self, footnotes):
        self.footnotes = footnotes

    def process_match(self, match):
        note_text = match.group(1)
        note_index = len(self.footnotes)
        self.footnotes.append(note_text)
        return self.markup.format(note_index, note_index + 1)


class EmphasisParser(ElementParser):
    label_type = u'\u7740\u91cd'
    markup = Markup(u'<strong class="emphasis">{0}</strong>')

    def process_match(self, match):
        return self.markup.format(match.group(1))


class MetadataParser(object):
    """The parser to processing paragraphs with metadata."""

    markup = Markup(
        u'<sup id="footnote-ref-{0}">'
        u'<a class="footnote via-m" href="#footnote-{0}">[{1}]</a></sup>')

    def __init__(self, footnotes, images):
        self.footnotes = footnotes
        self.images = images

    def parse(self, value):
        if not isinstance(value, list):
            return value
        fragments = self.make_fragments(value)
        return Markup(u'\n').join(fragments)

    def make_fragments(self, value):
        for fragment in value:
            kind = fragment['kind']
            content = fragment['content']
            if kind == 'plaintext':
                yield Markup(u'<span>{0}</span>').format(content)
            elif kind == 'footnote':
                index = len(self.footnotes)
                self.footnotes.append(content)
                yield self.markup.format(index, index + 1)
            elif kind == 'emphasize':
                yield Markup(u'<strong>{0}</strong>').format(content)
            elif kind == 'latex':
                template = Markup(
                    u'<img alt="{0}" src="Image/{1}" class="formula {2}" />')
                for fmt in ['svg', 'png']:
                    image = LaTexImage(
                        content, output_format=fmt, output_dpi=300)
                    self.images[image.filename] = image
                    yield template.format(content, image.image_filename, fmt)
            elif kind == 'code':
                yield Markup(u'<code class="code">{0}</code>').format(content)
            else:
                raise ValueError('unknown kind: %r' % kind)


class LaTexImage(object):
    """The inline image for rendering LaTex formula."""

    def __init__(self, latex, output_format, output_dpi):
        self.latex = latex
        self.output_format = output_format
        self.output_dpi = output_dpi
        self.image = render_latex(latex, format_=output_format, dpi=output_dpi)

    @property
    def filename(self):
        return 'latex-{0}-{1}'.format(sha1(self.latex), self.output_format)

    @property
    def image_filename(self):
        return '{0}.{1}'.format(self.filename, self.output_format)

    @property
    def image_filepath(self):
        return 'Content/Image/' + self.image_filename

    @property
    def image_bytes(self):
        return self.image.read()
