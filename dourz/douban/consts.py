HOME_URL = "http://www.douban.com/"
MINE_URL = "http://www.douban.com/mine/"

LOGIN_URL = "https://www.douban.com/accounts/login"
LOGIN_BTN_TEXT = u"\u767b\u5f55"

READER_BOOK_URL = "https://read.douban.com/reader/ebook/{aid}/"
READER_BOOKSHELF_URL = "https://read.douban.com/j/articles/bookshelf"
READER_DATA_URL = "https://read.douban.com/j/article_v2/get_reader_data"

BROWSER_USERAGENTS = [
    ("Mozilla/5.0 (X11; Linux x86_64; rv:27.0) Gecko/20100101 Firefox/27.0"),
    ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 "
     "(KHTML, like Gecko) Chrome/30.0.1599.101"),
    ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 "
     "(KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36"),
    ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:25.0) Gecko/20100101 "
     "Firefox/25.0"),
    ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9) AppleWebKit/537.71 "
     "(KHTML, like Gecko) Version/7.0 Safari/537.71"),
    ("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) "
     "Chrome/30.0.1599.114 Safari/537.36"),
]
