from __future__ import absolute_import

import datetime

from jinja2 import Environment, PackageLoader
from markupsafe import Markup

from dourz.utils import sha1, without_duplicate_items


class Jinja2(object):
    """The utility for using jinja2."""

    options = {'autoescape': True}

    def __init__(self, package_name, template_folder='templates'):
        self.loader = PackageLoader(package_name, template_folder)
        self.environment = Environment(loader=self.loader, **self.options)
        self.environment.filters.update(self.make_custom_filters())

    def render_template(self, template_name, **data):
        template = self.environment.get_template(template_name)
        return template.render(**data)

    def make_custom_filters(self):
        return {'from_timestamp': datetime.datetime.fromtimestamp,
                'to_sha1': sha1,
                'format_styles': format_styles,
                'render_legend': render_legend,
                'without_duplicate_items': without_duplicate_items}


def format_styles(fmt):
    def generator():
        yield 'fmt-align-%s' % fmt['p_align']
        if fmt['p_bold']:
            yield 'fmt-bold'
        if fmt.get('p_center'):
            pass  # I don't know what it is.
        if fmt['p_quote']:
            yield 'fmt-quote'
        if fmt['p_indent']:
            yield 'fmt-indent'
    return ' '.join(generator())


def render_legend(content):
    if isinstance(content, list):
        fragments = []
        for item in content:
            if item['type'] == 'paragraph':
                fragments.append(Markup(u'<p>{0}</p>').format(
                    item['data']['text']))
            else:
                raise ValueError('unknown type %r' % item['type'])
        return Markup(u'\n').join(fragments)
    return content
