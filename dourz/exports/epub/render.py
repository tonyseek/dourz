import uuid

from dourz.exports.html.render import BookRender, OfflineImage, download_image
from dourz.exports.jinja2 import Jinja2
from dourz.utils import safe_to_bytes, without_duplicate_items


class BookContext(object):
    """The book context with metadata and more."""

    jinja2 = Jinja2(__package__, 'templates')
    metadata_files = ['content.opf', 'toc.ncx', 'container.xml', 'mimetype']

    def __init__(self, book, book_data=None):
        self.uuid = uuid.uuid4()
        self.book = book
        self.book_data = book_data or book.data_as_dict()
        self.book_render = BookRender(self.book_data)
        self._chapters = None
        self._metadata = None

    @property
    def chapters(self):
        if self._chapters is None:
            self._chapters = list(self.make_chapters())
        return self._chapters

    def make_chapters(self):
        for index, chapter in enumerate(self.book_render.chapters):
            html = self.book_render.render_chapter(chapter)
            yield Chapter(index, chapter, html)

    @property
    def metadata(self):
        if self._metadata is None:
            self._metadata = dict(self.render_metadata())
        return self._metadata

    def render_metadata(self):
        for filename in self.metadata_files:
            text = self.jinja2.render_template(filename, ctx=self)
            yield filename, safe_to_bytes(text)

    def render_all(self):
        # the epub metadata
        yield 'mimetype', self.metadata['mimetype']
        yield 'META-INF/container.xml', self.metadata['container.xml']
        yield 'Content/toc.ncx', self.metadata['toc.ncx']
        yield 'Content/content.opf', self.metadata['content.opf']
        # the chapters
        for chapter in self.chapters:
            yield chapter.xhtml_filepath, safe_to_bytes(chapter.html)
            # the embed images
            for image in without_duplicate_items(
                    chapter.iter_offline_images(), 'image_filepath'):
                yield image.image_filepath, image.image_bytes
        # the inline images
        for image in without_duplicate_items(
                self.book_render.inline_images, 'image_filepath'):
            yield image.image_filepath, image.image_bytes
        # the cover image
        cover_url = self.book.cover_url.replace('/cut/', '/large/')
        yield 'Content/Image/cover.jpg', download_image(cover_url)


class Chapter(object):
    """The rendered chapter."""

    def __init__(self, index, chapter, html):
        self.index = index
        self.chapter = chapter
        self.html = html
        self.cached_images = {}

    @property
    def filename(self):
        return 'chapter-%d' % self.index

    @property
    def xhtml_filename(self):
        return '%s.xhtml' % self.filename

    @property
    def xhtml_filepath(self):
        return 'Content/' + self.xhtml_filename

    def iter_sections(self):
        """The sections of this chapter.

        The yield value is 2-tuple: (section_anchor_hash, section_title)
        """
        for content in self.chapter['contents']:
            # choice headlines as section title
            if content['type'] == 'headline':
                yield content['id'], strip_metadata(content['data']['text'])

    def iter_offline_images(self):
        """The images included in this chapter.

        The yield value is the instance of OfflineImageInformation
        """
        for content in self.chapter['contents']:
            # choice image urls
            if content['type'] == 'illus':
                id_ = content['id']
                if id_ not in self.cached_images:
                    self.cached_images[id_] = OfflineImage(content['data'])
                yield self.cached_images[id_]


def strip_metadata(text):
    if isinstance(text, list):
        return u''.join(fragment['content'] for fragment in text
                        if fragment['kind'] == 'plaintext')
    return text
